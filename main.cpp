#include "mainwindow.h"
#include <QCommandLineOption>
#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.setApplicationDescription("Check and Hold status of client");

    QCommandLineOption optionConfig
    (
        QStringList() << "config",
        QCoreApplication::translate("main", "config"),
        QCoreApplication::translate("main", "config")
    );

    parser.addOption(optionConfig);
    parser.process(a);

    QString argConfigs =
    (
        parser.value(optionConfig).size()>0 ?
        parser.value(optionConfig) : optionConfig.valueName() // metadata inside
    );
//    NOTE: когда между аргументами больше одного пробела, часть аргументов теряется
    MainWindow w(argConfigs.split(" "));
    if(!parser.value(optionConfig).size())
    {
        w.show();
    }
    return a.exec();
}
